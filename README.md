# README #
just run below code in terraform to get started with this module
module mydemo{
  source = "git::https://Kirangudekar@bitbucket.org/Kirangudekar/demo-module.git?ref=v0.0.2"
  vpc_id = "vpc-XXXX"
  cert  =  "arn:aws:acm:us-east-2:471443557195:certificate/ABCD/ABCD/ABCD"
  Subnet1_CIDR  =  "172.X.X.0/18"
  Subnet2_CIDR  =  "172.X.X.0/20"
  instance_key  =  "Your Private Key.PEM"
  instancetype  =  "t2.micro/t2.large/t2.nano etc"
  ami_ID        =  "ami-00-AMI"
}
provider aws{
region = "region name to create the infra"
}

### What is this repository for? ###

This repository has a module code which can be used to setup a quick environment with EC2,Subnet and Load balancer with Apache running on port 443.

* Version :
0.0.2

### How do I get set up? ###

* Summary of set up :
Download the repository using : 
git clone https://Kirangudekar@bitbucket.org/Kirangudekar/demo-module.git

* Configuration
use the configuration block like below to setup modules.
module mydemo{
  source = "git::https://Kirangudekar@bitbucket.org/Kirangudekar/demo-module.git?ref=v0.0.2"
  vpc_id = "vpc-XXXX"
  6 more variables to set up here
}
provider aws{
region = "region name to create the infra"
}

* How to run tests
terraform init
teraform apply


### Who do I talk to? ###

* Repo owner or admin
kirangudekar100@gmail.com