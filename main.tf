##### terraform backend #######
terraform{
  required_version = ">0.12"
  required_providers{
    aws = "~> 3.0"
  }
}

###### VPC and Subnet ###########
data "aws_vpc" "demo-vpc" {
  id = var.vpc_id
}

resource "aws_subnet" "subnet_1"{
  vpc_id  = data.aws_vpc.demo-vpc.id
  cidr_block = var.Subnet1_CIDR
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "subnet_1"
    product_id = "test_product"
  }
}

resource "aws_subnet" "subnet_2"{
  vpc_id  = data.aws_vpc.demo-vpc.id
  cidr_block = var.Subnet2_CIDR
  availability_zone = "us-east-2b"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "subnet_2"
    product_id = "test_product"
  }
}
#### EC2 instance #############
resource "aws_instance" "myec2" {
  ami = var.ami_ID
  instance_type = var.instancetype
  key_name  = var.instance_key
  iam_instance_profile = aws_iam_instance_profile.test_profile.name
  subnet_id = aws_subnet.subnet_1.id
  vpc_security_group_ids = [ aws_security_group.instance_SG.id ]
  user_data = "${file("${path.module}/user-data.sh")}"
  tags = {
        Name = "MyDemoEC2Instance"
        product_id = "test_product"
  }
}

####### Security Groups ############
resource "aws_security_group" "instance_SG" {
    vpc_id = data.aws_vpc.demo-vpc.id

    ingress {
      description = "HTTPS Allowed"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
    #  cidr_blocks = ["0.0.0.0/0"]
      security_groups = [aws_security_group.CLB_security_group.id]
    }
    egress {
      description = "Outbound Allow all traffic"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
          Name = "test_SG_1"
          product_id = "test_product"
    }
    }

    resource "aws_security_group" "CLB_security_group"{
      vpc_id = data.aws_vpc.demo-vpc.id
      tags = {
        Name = "Load Balancer Security Group"
        product_id = "test_product"
      }

        ingress {
        description = "Allow all traffic"
        from_port   = 0
        to_port     = 65535
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
      egress {
        description = "Outbound Allow all traffic"
        from_port   = 0
        to_port     = 65535
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
}

############ Load Balancer ##########
resource "aws_elb" "myclb" {
name               = "test-elb"
security_groups = [aws_security_group.CLB_security_group.id]
subnets = [aws_subnet.subnet_1.id, aws_subnet.subnet_2.id]



listener {
  instance_port     = 443
  instance_protocol = "https"
  lb_port           = 443
  lb_protocol       = "https"
  ssl_certificate_id = var.cert
}

health_check {
  healthy_threshold   = 2
  unhealthy_threshold = 2
  timeout             = 3
  target              = "HTTPS:443/"
  interval            = 5
}

instances                   = [aws_instance.myec2.id]
cross_zone_load_balancing   = true
idle_timeout                = 400
connection_draining         = true
connection_draining_timeout = 400

tags = {
  Name = "test_elb"
  product_id = "test_product"
}
}

output "myclb_dns_name"{
  value = aws_elb.myclb.dns_name
}

resource "aws_lb_target_group" "lb_target_group" {
name     = "tf-example-lb-tg"
port     = 443
protocol = "HTTPS"
vpc_id   = data.aws_vpc.demo-vpc.id
}

########### S3 , Role and Policy #############
resource "aws_s3_bucket" "test_bucket" {
  bucket = "testbucket-kirang"
  acl = "private"
  tags = {
    Name = "test_bucket"
    product_id = "test_product"
  }
  }

data "aws_iam_policy_document" "Policy_Document" {
  statement {
    actions   = ["s3:*"]
    resources = ["arn:aws:s3:::testbucket-kirang"]
    effect = "Allow"
  }
}

resource "aws_iam_policy" "test_policy" {
   name        = "test-policy"
   description = "My test policy"
   policy = data.aws_iam_policy_document.Policy_Document.json
}



resource "aws_iam_role" "instance" {
  name               = "Test2_role"
  path               = "/"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
  tags = {
    Name = "Test_Role"
    product_id = "test_product"
  }
}

resource "aws_iam_instance_profile" "test_profile" {
  name = "test_profile"
  role = aws_iam_role.instance.name
}


resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.instance.name
  policy_arn = aws_iam_policy.test_policy.arn
}
