variable "vpc_id" {
  description =  "VPC ID to use to create this infra."
  type        =   string
  default     =   ""
  }
variable "cert" {
  description =  "ACM certificate to be used for SSL"
  type        =   string
  default     =   ""
  }
variable "Subnet1_CIDR"{
  description =  "Valid CIDR to use in subnet1"
  type        =   string
  default     =   "0.0.0.0/0"
}
variable "Subnet2_CIDR"{
  description =  "Valid CIDR to use in subnet2"
  type        =   string
  default     =   "0.0.0.0/0"
}
variable "instance_key"{
  description =  "private key name to launch ec2 instance"
  type        =   string
  default     =   ""
}
variable "instancetype"{
  description =  "AWS instance type to launch"
  type        =   string
  default     =   ""
}
variable "ami_ID"{
  description =  "Provide AMI ID to use to launc an EC2 instance"
  type        =   string
  default     =   ""
}